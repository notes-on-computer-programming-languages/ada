# ada

Structured, statically typed, imperative, and object-oriented.
* https://adaic.org
* https://ada-lang.io

* [*Learn X in Y minutes, Where X=Ada*
  ](https://learnxinyminutes.com/docs/ada/)

# Tools
## ALIRE: Ada LIbrary REpository
* [alire.ada.dev](https://alire.ada.dev)
* [Repology](https://repology.org/project/alire/versions)

## SPARK
* [adacore.com/about-spark](https://adacore.com/about-spark)
* [*Introduction To SPARK*
  ](https://learn.adacore.com/courses/intro-to-spark)
* [github.com/AdaCore/spark2014](https://github.com/AdaCore/spark2014)
* [ada spark functional programming](https://google.com/search?q=ada+spark+functional+programming)
---
* [*Ada/SPARK: should I be using GNATprove? Where can I find it?*
  ](https://stackoverflow.com/questions/70491800/ada-spark-should-i-be-using-gnatprove-where-can-i-find-it)
  (2021) (Stack Overflow)
* [*Rust and SPARK: Software Reliability for Everyone*
  ](https://www.electronicdesign.com/markets/automation/article/21804924/rust-and-spark-software-reliability-for-everyone)
  2020-04 Quentin Ochem

# Official documentation
## AdaCore
* [adacore.com](https://adacore.com)
* [github.com/AdaCore](https://github.com/AdaCore)
* [*LEARN.ADACORE.COM*](https://learn.adacore.com)
  * [*Classes and Object Oriented Programming*
    ](https://learn.adacore.com/courses/Ada_For_The_CPP_Java_Developer/chapters/08_Classes_and_Object_Oriented_Programming.html)
* [*Introduction to Ada*
  ](https://learn.adacore.com/courses/intro-to-ada)
## Ada Conformity Assessment Authority (ACAA)
* [*ACAA - Standards*
  ](http://ada-auth.org/standards/)
* [*Ada 2022 Language Reference Manual*
  ](http://ada-auth.org/standards/ada22.html)

# Books
* [*Beginning Ada Programming: From Novice to Professional*
  ](https://www.oreilly.com/library/view/beginning-ada-programming/9781484254288/)
  2019-12 Andrew T. Shvets (Apress)
* [*Building High Integrity Applications with SPARK*
  ](https://www.cambridge.org/core/books/building-high-integrity-applications-with-spark/F213D9867D2E271F5FF3EDA765D48E95)
  2015-10 John W. McCormick, University of Northern Iowa, Peter C. Chapin, Vermont Technical College (Cambridge University Press)
  * [WorldCat](https://worldcat.org/en/search?q=Building+High+Integrity+Applications+with+SPARK)
* [*Programming in Ada: A First Course*
  ](https://www.cambridge.org/ca/universitypress/subjects/computer-science/programming-languages-and-applied-logic/programming-ada-first-course?format=HB&isbn=9780521257282)
  1985-05 Robert G. Clark (Cambridge University Press: Out of Print)

# Unofficial documentation
* [*Ada Programming*
  ](https://en.wikibooks.org/wiki/Ada_Programming)
* [*Ada (programming language)*
  ](https://en.m.wikipedia.org/wiki/Ada_(programming_language))
* [*GNAT*](https://en.m.wikipedia.org/wiki/GNAT)
* https://rosettacode.org/wiki/Higher-order_functions
* [*Programming SPARK 2014*
  ](https://www.classcentral.com/course/independent-spark-2014-5969)
  AdaCore University
---
* [*Ada on Windows and Linux: an installation guide*
  ](https://www.noureddine.org/articles/ada-on-windows-and-linux-an-installation-guide)
  2022-11 Adel Noureddine
* [*Things You Can Do in Ada But Can't Do in Most Other Programming Languages*
  ](https://www.linkedin.com/pulse/things-you-can-do-ada-cant-most-other-programming-languages-john-yu)
  2022-06 John Yu (Linkedin)
* [*Why Ada Is The Language You Want To Be Programming Your Systems With*
  ](https://hackaday.com/2019/09/10/why-ada-is-the-language-you-want-to-be-programming-your-systems-with/)
  2019-09 Maya Posch
* [*Higher Order Functions in Ada, Part 1: Assigning a Function*
  ](https://www.youtube.com/watch?v=WY-CiZnyesA)
  2018 Patrick Kelly (YouTube)
* [*Higher Order Functions in Ada, Part 2: Functors and Callbacks*
  ](https://www.youtube.com/watch?v=qlmo85ZppAg)
  2018 Patrick Kelly (YouTube)
* [*Scoping, higher-order functions, and Ada*
  ](https://kronopath.net/nyu/pl-notes/feb-10-lecture/)
  2016-02 Gabriel B. Nunes (Kronopath)

## Functional programming
* [ada functional programming
  ](https://www.google.com/search?q=ada+functional+programming)
---
* [*Exploring the boundaries of Ada syntax with functional-style iterators*
  ](https://www.researchgate.net/publication/349330900_Exploring_the_boundaries_of_Ada_syntax_with_functional-style_iterators)
  2021-02 Alejandro R. Mosteo (Journal of Systems Architecture)
* [*Blog - Programming Paradigms for Ada*
  ](http://www.argonauts-it.org/blogs/programming_paradigms_for_ada.html)
  2019-12 Didier Willame (Argonauts-iT)
*  [*Functional Programming in...Ada?*
  ](http://okasaki.blogspot.com/2008/07/functional-programming-inada.html)
  2008-07 Chris Okasaki
  * [*Ada, the Ultimate Lambda?*
    ](http://lambda-the-ultimate.org/node/2897)
* [*Full functional programming in a declarative Ada dialect*
  ](https://dl.acm.org/doi/pdf/10.1145/143557.143975)
  1992-12 Paul A. Bailes, Dan Johnston, Eric Salzman, Li Wang

# Features
## Memory management
### Garbage collection
* [ada garbage collection](https://google.com/search?q=ada+garbage+collection)

### Resource acquisition is initialization (RAII)
* [ada resource acquisition is initialization RAII
  ](https://google.com/search?q=ada+resource+acquisition+is+initialization+RAII)
